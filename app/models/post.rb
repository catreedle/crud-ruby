class Post < ApplicationRecord

    validates :title, presence: true, allow_nil: false,
    validates :content, presence: true, allow_nil: false,    
end
