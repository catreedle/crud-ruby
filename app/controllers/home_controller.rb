class HomeController < ApplicationController
    def index
        render json: {
            success: true,
            message: "Server Up!"
        }
    end
end
