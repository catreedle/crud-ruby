class PostsController < ApplicationController
  def create
    post = Post.create({
      :title => params[:title],
      :content => params[:content]
    })
    return render json: {
      success: true,
      data: post
      }  if post.save!

    render json: {
      success: false,
      errors: "Failed to create post!"
    }
  end

  def index
    @post = Post.all
    return render json: {
      success: true,
      data: @post
    }
  end

  def update
    post = Post.find(params[:id])
    post.update(params.permit(:title, :content))

    return render json: {
      success: true,
      data: post
    } if post.save!

    render json: {
      success: false,
      errors: "Failed to update"
    }
  end

  def show
    post = Post.find(params[:id])

    return render json: {
      success: true,
      data: post
    }
  end

  def delete
    post = Post.find(params[:id])
    post.destroy

    return render json: {
      success: true,
      data: "Successfully deleted post!"
    }
  end
end
