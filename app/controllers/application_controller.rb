class ApplicationController < ActionController::API
rescue_from ActiveRecord::RecordNotFound, with: :format_response

    private

    def format_response
        render json: {
            success: false,
            errors: "Data not found!"
        }
    end
end
