Rails.application.routes.draw do
  get '/home' => "home#index"
  post '/posts' => "posts#create"
  get '/posts' => "posts#index"
  get '/posts/:id' => "posts#show"
  put '/posts/:id' => "posts#update"
  delete '/posts/:id' => "posts#delete"
end
